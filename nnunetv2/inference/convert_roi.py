from nnunetv2.paths import nnUNet_preprocessed, nnUNet_raw ####   Imports the paths I need here
from batchgenerators.utilities.file_and_folder_operations import load_json, join
from nnunetv2.utilities.dataset_name_id_conversion import maybe_convert_to_dataset_name
import SimpleITK as sitk
import numpy as np
import os

class format_converter:

    def __init__ (self, img_path, out_path, dataset_id, agent_name):

        self.img_path = img_path
        self.out_path = out_path
        self.dataset_id = dataset_id
        self.agent_name = agent_name
        self.roi_path = os.path.join(self.out_path, "img.nii.gz")
        self.dataset_folder_base = join(nnUNet_raw, maybe_convert_to_dataset_name(self.dataset_id))
        self.dataset_file = join(self.dataset_folder_base, 'dataset.json')
        self.dataset_file = load_json(self.dataset_file)
        self.channels = list(self.dataset_file['channel_names'].keys())
        self.labels = len(list(self.dataset_file['labels'].keys()))

        # self.preprocessed_dataset_folder_base = join(nnUNet_preprocessed, maybe_convert_to_dataset_name(dataset_id)) # Dataset number is given in the CLI, change it to that
        # self.plans_file = join(self.preprocessed_dataset_folder_base, 'nnUNetPlans.json')
        # self.plans = load_json(self.plans_file) # Used before to get label, not needed anymore

        self.image = sitk.ReadImage(self.img_path)

    def convert_to_nii(self):

        img_list = []
        # I hate this so much
        temp_folder = os.path.join(self.out_path, f'{self.agent_name}')
        os.makedirs(temp_folder, exist_ok=True)

        for channel in self.channels:

            channel_img_path = os.path.join(temp_folder, f'img_000{channel}.nii.gz')
            img_list.append(channel_img_path)
            sitk.WriteImage(self.image, fileName=channel_img_path)
        
        return img_list

    # def convert_to_roi(self):

    #     self.roi_arr = sitk.GetArrayFromImage(sitk.ReadImage(self.roi_path))
    #     for label in range(self.labels):

    #         if label == 0:
    #             continue

    #         roi = (self.roi_arr == label).astype(int)
    #         save_array_as_sitk(original_img_obj = self.image, array = roi, output_path = os.path.join(self.out_path, f'{self.dataset_id}_{label}.nii.gz'))
